from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.db import transaction
from django.utils.translation import ugettext as _
from rest_framework import serializers

from app.models import Product, Store, Pantry, PantryProduct, PantryUser, PantryProductWithdraw

User = get_user_model()


class BaseSerializer(serializers.ModelSerializer):
    id = serializers.UUIDField(read_only=True)


class CreateWithCreatedByMixin:
    # noinspection PyUnresolvedReferences
    def create(self, validated_data):
        validated_data['created_by'] = self.context['request'].user
        return super().create(validated_data)


class PantrySerializer(BaseSerializer):
    def validate(self, attrs):
        user = self.context['request'].user
        user_has_pantry_with_same_name = user.pantries_relations.filter(pantry__name__iexact=attrs['name']).exists()

        if user_has_pantry_with_same_name:
            raise serializers.ValidationError(_('You already have a pantry with this name.'))

        return super().validate(attrs)

    def create(self, validated_data):
        user = self.context['request'].user

        with transaction.atomic():
            pantry_instance = super().create(validated_data)
            PantryUser.objects.create(user=user, pantry=pantry_instance, is_admin=True)

        return pantry_instance

    class Meta:
        model = Pantry
        fields = ('id', 'name')


class ProductSerializer(CreateWithCreatedByMixin, BaseSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name', 'bar_code', 'image')


class StoreSerializer(CreateWithCreatedByMixin, BaseSerializer):
    class Meta:
        model = Store
        fields = ('id', 'name')


class PantryProductSerializer(serializers.ModelSerializer):
    id = serializers.CharField(source='product.id', read_only=True)
    name = serializers.CharField(source='product.name', read_only=True)
    image = serializers.ImageField(source='product.image', read_only=True)
    product = serializers.PrimaryKeyRelatedField(queryset=Product.objects.all(), write_only=True)
    bought_at = serializers.DateField(write_only=True, required=False)
    expiration_date = serializers.DateField(write_only=True, required=False)

    def create(self, validated_data):
        validated_data['pantry'] = self.context['pantry']
        return super().create(validated_data)

    class Meta:
        model = PantryProduct
        fields = (
            'quantity',
            # Read-Only
            'id',
            'name',
            'image',
            # Write-Only
            'product',
            'bought_at',
            'expiration_date',
        )


class PantryProductWithdrawSerializer(serializers.ModelSerializer):
    pantry = serializers.PrimaryKeyRelatedField(read_only=True)
    product = serializers.PrimaryKeyRelatedField(read_only=True)

    def _get_quantity_available(self):
        if not hasattr(self, '_quantity_available'):
            _filter = {
                'pantry': self.context['pantry'],
                'product': self.context['product'],
            }
            quantity = PantryProduct.objects.get_quantity_sum(**_filter)
            quantity_withdrawn = PantryProductWithdraw.objects.get_quantity_sum(**_filter)
            self._quantity_available = quantity - quantity_withdrawn
        return self._quantity_available

    def validate(self, attrs):
        quantity_available = self._get_quantity_available()

        if quantity_available == 0:
            raise serializers.ValidationError(_('This product is not in your pantry.'))

        return attrs

    def validate_quantity(self, quantity):
        if quantity == 0:
            raise serializers.ValidationError(
                _('Is not possible to withdraw nothing. Please, send a positive quantity.'),
            )

        quantity_available = self._get_quantity_available()
        if quantity_available and quantity_available - quantity < 0:
            raise serializers.ValidationError(
                _('There is only {} of this product to withdraw.').format(quantity_available),
            )

        return quantity

    def create(self, validated_data):
        validated_data['pantry'] = self.context['pantry']
        validated_data['product'] = self.context['product']
        return super().create(validated_data)

    class Meta:
        model = PantryProductWithdraw
        fields = ('product', 'pantry', 'quantity')


class UserSerializer(BaseSerializer):
    password = serializers.CharField(write_only=True)

    def create(self, validated_data):
        validated_data['password'] = make_password(validated_data['password'])
        return super().create(validated_data)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
