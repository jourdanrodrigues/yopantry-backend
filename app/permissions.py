from rest_framework.permissions import BasePermission


class NestedPantryOwner(BasePermission):
    def has_permission(self, request, view) -> bool:
        return request.user.pantries_relations.filter(pantry_id=view.kwargs['pantry_pk']).exists()
