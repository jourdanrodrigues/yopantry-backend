from unittest.mock import MagicMock

from django.core.handlers.wsgi import WSGIRequest
from django.db.models import QuerySet
from django.test import SimpleTestCase
from django.utils.translation import ugettext as _
from rest_framework.exceptions import ValidationError
from rest_framework.test import APIRequestFactory

from app.filters import NameFilter


def get_request(query_params: dict) -> WSGIRequest:
    request = APIRequestFactory().get('/')
    request.query_params = query_params
    return request


class TestFilterQueryset(SimpleTestCase):
    error_message = 'Filtering by "name" is required with at least 3 characters.'

    def test_when_get_query_set_has_name_filter_with_less_than_3_characters_raises_validation_error(self):
        request = get_request({'name': 'ab'})
        queryset = QuerySet()

        with self.assertRaisesMessage(ValidationError, _(self.error_message)):
            NameFilter().filter_queryset(request, queryset, None)

    def test_when_get_query_set_does_not_has_name_filter_raises_validation_error(self):
        request = get_request({})  # empty query params
        queryset = QuerySet()

        with self.assertRaisesMessage(ValidationError, _(self.error_message)):
            NameFilter().filter_queryset(request, queryset, None)

    def test_when_get_query_set_has_name_filter_with_more_than_3_characters_returns_queryset(self):
        request = get_request({'name': 'four'})
        queryset = QuerySet()
        another_queryset = QuerySet()
        queryset.filter = MagicMock(return_value=another_queryset)

        queryset_filtered = NameFilter().filter_queryset(request, queryset, None)

        self.assertIs(queryset_filtered, another_queryset)
