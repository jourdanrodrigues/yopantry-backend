from tempfile import NamedTemporaryFile
from unittest import mock

from PIL import Image
from django.conf import settings
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from rest_framework import status
from rest_framework.test import APITestCase

from app.helpers import generate_jwt_token
from app.models import Product
from app.utils import UUID_REGEX

User = get_user_model()


def get_image_file():
    image_file = NamedTemporaryFile(suffix='.jpg')
    Image.new('RGB', (1, 1)).save(image_file)
    image_file.seek(0)
    return image_file


class TestPost(APITestCase):
    fixtures = ['users']

    def test_when_data_is_right_returns_saved_data_and_id_as_uuid_with_created_status(self):
        token = generate_jwt_token(User.objects.first())
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

        post_data = {
            'name': 'Product name',
            'bar_code': '1234567890',
            'image': get_image_file(),
        }

        storage_save_method = settings.DEFAULT_FILE_STORAGE + '.save'

        with mock.patch(storage_save_method, return_value='temp_file.jpg'):
            response = self.client.post('/api/products/', post_data)

        data = response.data

        expected_data = {
            'id': data.get('id'),
            'name': post_data['name'],
            'image': data.get('image'),  # refers to the path
            'bar_code': post_data['bar_code'],
        }

        self.assertDictEqual(data, expected_data)
        self.assertRegex(expected_data['id'], UUID_REGEX)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class TestGet(APITestCase):
    fixtures = ['users']

    def test_when_name_filter_has_3_or_more_characters_returns_product_list_filtered_with_created_status(self):
        user = User.objects.first()
        Product.objects.bulk_create([
            Product(name='Product 1', bar_code='1', created_by=user),
            Product(name='Product 2', bar_code='2', created_by=user),
            Product(name='A product 3', bar_code='3', created_by=user),
        ])

        response = self.client.get('/api/products/', {'name': 'a produ'})

        self.assertEqual(len(response.data), 1)  # Right amount of items filtered

        data = response.data[0]

        self.assertIn('id', data)
        self.assertIn('name', data)
        self.assertIn('image', data)
        self.assertIn('bar_code', data)
        self.assertEqual(len(data), 4)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_when_name_filter_has_less_than_3_characters_returns_bad_request_status(self):
        response = self.client.get('/api/products/')

        self.assertEqual(response.data, [_('Filtering by "name" is required with at least 3 characters.')])
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
