from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from rest_framework import status
from rest_framework.test import APITestCase

from app.helpers import generate_jwt_token
from app.models import Pantry, Product, PantryProduct, PantryProductWithdraw

User = get_user_model()


class TestGet(APITestCase):
    fixtures = ['users', 'pantry_user_relations', 'pantries', 'pantry_product_relations', 'products']

    def __authenticate_user(self, user: User):
        token = generate_jwt_token(user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

    def test_when_user_is_not_authenticated_returns_unauthorized_response(self):
        response = self.client.get('/api/pantries/0/products/')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_when_user_does_not_own_pantry_returns_forbidden_response(self):
        user = User.objects.first()
        self.__authenticate_user(user)

        pantry = Pantry.objects.create(name='New pantry')

        response = self.client.get('/api/pantries/{}/products/'.format(pantry.id))

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_when_there_is_withdraw_then_returns_correct_quantity(self):
        user = User.objects.that_has_pantries_with_products().first()
        self.__authenticate_user(user)

        pantry = user.pantries_relations.select_related('pantry').first().pantry
        pantry_product = pantry.products_relations.first()

        pantry_product_withdraw = PantryProductWithdraw.objects.create(
            pantry=pantry,
            product=pantry_product.product,
            quantity=1,
        )

        response = self.client.get('/api/pantries/{}/products/'.format(pantry.id))

        quantity = response.data[0]['quantity']
        expected_quantity = pantry_product.quantity - pantry_product_withdraw.quantity

        self.assertEqual(quantity, expected_quantity)

    def test_that_it_returns_correct_data(self):
        user = User.objects.that_has_pantries_with_products().first()
        self.__authenticate_user(user)
        pantry_id = user.pantries_relations.values_list('pantry_id', flat=True).first()

        response = self.client.get('/api/pantries/{}/products/'.format(pantry_id))
        data = response.data[0]

        self.assertIn('id', data)
        self.assertIn('name', data)
        self.assertIn('image', data)
        self.assertIn('quantity', data)
        self.assertEqual(len(data), 4)
        self.assertIsInstance(response.data, list)

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestPost(APITestCase):
    fixtures = ['users', 'pantry_user_relations', 'pantries', 'pantry_product_relations', 'products']

    def setUp(self):
        product_1_id, product_2_id = Product.objects.values_list('id', flat=True)[:2]
        self.data = [
            {'product': product_1_id, 'quantity': 2},
            {'product': product_2_id, 'quantity': 5},
        ]

    def __authenticate_and_get_pantry(self):
        user = User.objects.with_pantries().first()
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + generate_jwt_token(user))
        return user.pantries_relations.values_list('pantry_id', flat=True).first()

    def test_when_data_is_valid_then_returns_created_status(self):
        pantry_id = self.__authenticate_and_get_pantry()

        response = self.client.post('/api/pantries/{}/products/'.format(pantry_id), self.data, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_when_data_is_valid_then_returns_correct_data(self):
        pantry_id = self.__authenticate_and_get_pantry()

        response = self.client.post('/api/pantries/{}/products/'.format(pantry_id), self.data, format='json')
        data = response.data[0]

        self.assertIn('id', data)
        self.assertIn('name', data)
        self.assertIn('image', data)
        self.assertIn('quantity', data)
        self.assertEqual(len(data), 4)
        self.assertIsInstance(response.data, list)

    def test_when_data_is_valid_then_create_pantry_products_entries(self):
        pantry_id = self.__authenticate_and_get_pantry()

        queryset = PantryProduct.objects.all()
        queryset_count = queryset.count()

        self.client.post('/api/pantries/{}/products/'.format(pantry_id), self.data, format='json')

        self.assertEqual(queryset_count + len(self.data), queryset.count())


class TestDelete(APITestCase):
    fixtures = ['users', 'pantry_user_relations', 'pantries', 'pantry_product_relations', 'products']

    def __authenticate_user(self, user: User):
        token = generate_jwt_token(user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

    def test_when_data_is_valid_then_create_pantry_products_withdraw_entry_and_returns_no_content_response(self):
        user = User.objects.that_has_pantries_with_products().first()
        self.__authenticate_user(user)

        pantry = user.pantries_relations.select_related('pantry').first().pantry
        pantry_product = pantry.products_relations.first()
        product_id = pantry_product.product_id

        delete_data = {
            'quantity': pantry_product.quantity,
        }

        queryset = PantryProductWithdraw.objects.all()
        queryset_count = queryset.count()

        endpoint = '/api/pantries/{}/products/{}/'.format(pantry.id, product_id)
        response = self.client.delete(endpoint, delete_data, format='json')

        self.assertEqual(queryset_count + 1, queryset.count())
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_when_quantity_is_zero_then_returns_bad_request_response(self):
        user = User.objects.that_has_pantries_with_products().first()
        self.__authenticate_user(user)

        pantry = user.pantries_relations.select_related('pantry').first().pantry
        pantry_product = pantry.products_relations.first()
        product_id = pantry_product.product_id

        delete_data = {
            'quantity': 0,
        }

        queryset = PantryProductWithdraw.objects.all()
        queryset_count = queryset.count()

        endpoint = '/api/pantries/{}/products/{}/'.format(pantry.id, product_id)
        response = self.client.delete(endpoint, delete_data, format='json')

        error_message = _('Is not possible to withdraw nothing. Please, send a positive quantity.')
        expected_data = {
            'quantity': [error_message],
        }

        self.assertDictEqual(expected_data, response.data)

        self.assertEqual(queryset_count, queryset.count())  # Make sure none was created
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_when_quantity_exceeds_available_then_returns_bad_request_response(self):
        user = User.objects.that_has_pantries_with_products().first()
        self.__authenticate_user(user)

        pantry = user.pantries_relations.select_related('pantry').first().pantry
        pantry_product = pantry.products_relations.first()
        product_id = pantry_product.product_id

        delete_data = {
            'quantity': pantry_product.quantity + 1,
        }

        queryset = PantryProductWithdraw.objects.all()
        queryset_count = queryset.count()

        endpoint = '/api/pantries/{}/products/{}/'.format(pantry.id, product_id)
        response = self.client.delete(endpoint, delete_data, format='json')

        error_message = _('There is only {} of this product to withdraw.').format(pantry_product.quantity)
        expected_data = {
            'quantity': [error_message],
        }

        self.assertDictEqual(expected_data, response.data)
        self.assertEqual(queryset_count, queryset.count())  # Make sure none was created
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_when_product_is_not_in_pantry_then_returns_bad_request_response(self):
        user = User.objects.that_has_pantries_with_products().first()
        self.__authenticate_user(user)

        pantry_id = user.pantries_relations.first().pantry_id
        product_id = Product.objects.create(name='name', bar_code='bar_code', created_by=user).id

        delete_data = {
            'quantity': 2,
        }

        queryset = PantryProductWithdraw.objects.all()
        queryset_count = queryset.count()

        endpoint = '/api/pantries/{}/products/{}/'.format(pantry_id, product_id)
        response = self.client.delete(endpoint, delete_data, format='json')

        expected_data = {
            'non_field_errors': [_('This product is not in your pantry.')],
        }

        self.assertDictEqual(expected_data, response.data)
        self.assertEqual(queryset_count, queryset.count())  # Make sure none was created
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
