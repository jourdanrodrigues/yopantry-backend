from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from rest_framework import status
from rest_framework.test import APITestCase

from app.helpers import generate_jwt_token
from app.models import Pantry, PantryUser

User = get_user_model()


class TestGet(APITestCase):
    fixtures = ['users', 'pantry_user_relations', 'pantries']

    def __authenticate_user(self, user: User):
        token = generate_jwt_token(user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

    def test_when_user_is_not_authenticated_returns_unauthorized_response(self):
        response = self.client.get('/api/pantries/')

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_that_it_returns_pantries_from_user(self):
        user = User.objects.with_pantries().first()
        self.__authenticate_user(user)
        Pantry.objects.create(name='Pantry with no relations')

        response = self.client.get('/api/pantries/')

        self.assertEqual(user.pantries_relations.count(), len(response.data))
        self.assertIsInstance(response.data, list)  # Sanity check
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_that_it_returns_correct_data(self):
        user = User.objects.with_pantries().first()
        self.__authenticate_user(user)
        Pantry.objects.create(name='Pantry with no relations')

        response = self.client.get('/api/pantries/')
        data = response.data[0]

        self.assertIn('id', data)
        self.assertIn('name', data)
        self.assertEqual(len(data), 2)


class TestPost(APITestCase):
    fixtures = ['users', 'pantry_user_relations', 'pantries']

    def __authenticate_user(self, user: User):
        token = generate_jwt_token(user)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

    def test_when_data_is_valid_then_returns_data_with_created_status(self):
        user = User.objects.create_user(username='User', password='Strong password')
        self.__authenticate_user(user)

        data = {'name': 'Pantry name'}

        response = self.client.post('/api/pantries/', data)

        self.assertIn('id', response.data)
        self.assertIn('name', response.data)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_when_data_is_valid_then_creates_relation_between_pantry_and_user(self):
        user = User.objects.create_user(username='User', password='Strong password')
        self.__authenticate_user(user)

        data = {'name': 'Pantry name'}

        response = self.client.post('/api/pantries/', data)

        pantry_id = response.data.get('id')
        relation_exists = PantryUser.objects.filter(user=user, pantry_id=pantry_id, pantry__name=data['name']).exists()
        self.assertTrue(relation_exists)

    def test_when_there_is_a_pantry_with_same_name_then_returns_bad_request_status(self):
        user = User.objects.with_pantries().prefetch_related('pantries_relations__pantry').first()
        self.__authenticate_user(user)

        pantry_name = user.pantries_relations.values_list('pantry__name', flat=True).first()
        data = {'name': pantry_name}

        response = self.client.post('/api/pantries/', data)

        self.assertDictEqual(response.data, {'non_field_errors': [_('You already have a pantry with this name.')]})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
