from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase

from app.helpers import generate_jwt_token
from app.models import Store
from app.utils import UUID_REGEX

User = get_user_model()


class TestPost(APITestCase):
    fixtures = ['users']

    def test_when_data_is_right_returns_saved_data_and_id_as_uuid_with_created_status(self):
        token = generate_jwt_token(User.objects.first())
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + token)

        post_data = {
            'name': 'Store name',
        }

        response = self.client.post('/api/stores/', post_data)
        data = response.data

        expected_data = {
            'id': data.get('id'),
            'name': post_data['name'],
        }

        self.assertDictEqual(data, expected_data)
        self.assertRegex(expected_data['id'], UUID_REGEX)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class TestGet(APITestCase):
    fixtures = ['stores', 'users']

    def test_when_name_with_more_than_3_characters_is_sent_returns_store_list_filtered(self):
        response = self.client.get('/api/stores/', {'name': 'store 1'})

        self.assertLess(len(response.data), Store.objects.count())
