from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase

User = get_user_model()


class TestPost(APITestCase):
    def test_when_data_is_valid_and_user_exists_returns_token_with_created_status(self):
        data = {
            'username': 'username',
            'password': 'Strong Password',
        }
        User.objects.create_user(**data)

        response = self.client.post('/api/auth/', data)

        self.assertDictEqual(response.data, {'token': response.data.get('token')})
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_when_data_is_valid_and_user_exists_returns_token(self):
        data = {
            'username': 'username',
            'password': 'Strong Password',
        }
        User.objects.create_user(**data)

        response = self.client.post('/api/auth/', data)

        self.assertDictEqual(response.data, {'token': response.data.get('token')})

    def test_when_data_is_valid_and_user_does_not_exist_returns_error(self):
        data = {
            'username': 'not_existing_user',
            'password': 'Strong Password',
        }

        response = self.client.post('/api/auth/', data)

        self.assertDictEqual(response.data, {'non_field_errors': ['Unable to log in with provided credentials.']})
