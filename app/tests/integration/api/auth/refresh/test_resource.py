from datetime import timedelta
from unittest import mock

from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_jwt.settings import api_settings

from app.helpers import generate_jwt_token

User = get_user_model()


class TestPost(APITestCase):
    def test_when_token_is_valid_returns_new_token_with_ok_status(self):
        user = User.objects.create_user(username='username', password='Strong Password')

        with mock.patch.object(api_settings, 'JWT_EXPIRATION_DELTA', timedelta(seconds=0)):
            token = generate_jwt_token(user)

        response = self.client.post('/api/auth/refresh/', {'token': token})
        new_token = response.data.get('token')

        self.assertNotEqual(new_token, token)
        self.assertIsInstance(new_token, str)  # Sanity check
        self.assertEqual(response.status_code, status.HTTP_200_OK)
