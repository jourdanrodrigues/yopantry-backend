from django.contrib.auth import get_user_model
from django.utils.translation import ugettext as _
from rest_framework import status
from rest_framework.test import APITestCase

User = get_user_model()


class TestPost(APITestCase):
    def test_when_data_is_valid_returns_new_token_with_ok_status(self):
        post_data = {
            'username': 'username',
            'email': 'email@test.com',
            'password': 'Strong Password',
        }

        response = self.client.post('/api/auth/register/', post_data)
        data = response.data

        self.assertIn('id', data)
        self.assertIn('email', data)
        self.assertIn('username', data)
        self.assertEqual(len(data), 3)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_when_data_is_from_existing_user_returns_new_token_with_ok_status(self):
        post_data = {
            'username': 'username',
            'email': 'email@test.com',
            'password': 'Strong Password',
        }

        User.objects.create_user(**post_data)

        response = self.client.post('/api/auth/register/', post_data)
        data = response.data

        self.assertEqual(data.get('email'), [_('A user with that email already exists.')])
        self.assertEqual(data.get('username'), [_('A user with that username already exists.')])
        self.assertEqual(len(data), 2)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
