from django.contrib.auth import get_user_model
from django.test import TestCase

from app.models import Pantry

User = get_user_model()


class PantryTestCase(TestCase):
    fixtures = ['users', 'pantry_user_relations', 'pantries', 'pantry_product_relations', 'products']

    def test_that_the_method_with_products_works(self):
        Pantry.objects.create(name='Pantry with no products')
        pantries = Pantry.objects.all()
        self.assertLess(pantries.with_products().count(), pantries.count())

    def test_that_the_method_filter_by_user_works(self):
        Pantry.objects.create(name='Pantry with no relations')

        user = User.objects.with_pantries().first()
        pantries = Pantry.objects.all()

        self.assertLess(pantries.filter_by_user(user).count(), pantries.count())
