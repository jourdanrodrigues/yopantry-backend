from django.contrib.auth import get_user_model
from django.test import TestCase

User = get_user_model()


class UserTestCase(TestCase):
    fixtures = ['users', 'pantry_user_relations', 'pantries']

    def test_that_the_method_with_pantries_works(self):
        users = User.objects.all()
        self.assertLess(users.with_pantries().count(), users.count())
