# UUID RegEx: https://stackoverflow.com/a/19989922/4694834
# Case insensitive: https://stackoverflow.com/a/10444271/4694834
UUID_REGEX = r'^(?i)[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$'
