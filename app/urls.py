from rest_framework.routers import DefaultRouter
from rest_framework_nested.routers import NestedDefaultRouter

from app.views import PantryViewSet, ProductViewSet, StoreViewSet, PantryProductViewSet, AuthViewSet

router = DefaultRouter()
router.register('stores', StoreViewSet)
router.register('pantries', PantryViewSet)
router.register('products', ProductViewSet)
router.register('auth', AuthViewSet, base_name='auth')

pantry_router = NestedDefaultRouter(router, 'pantries', lookup='pantry')
pantry_router.register('products', PantryProductViewSet)
