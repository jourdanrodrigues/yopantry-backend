from datetime import date
from uuid import UUID

from django.db import models
from django.db.models import QuerySet, Sum, F
from django.utils.translation import ugettext as _

from app.models._base import BaseModel
from app.models._models import Product
from app.models._pantry import Pantry


class PantryProductQuerySet(QuerySet):
    def get_quantity_sum(self, pantry: Pantry or UUID, product: Product or UUID) -> int:
        quantity_sum = (
            self.filter(pantry=pantry, product=product)
                .annotate(Sum('quantity'))
                .values('quantity__sum')
                .annotate(quantity=F('quantity__sum'))
                .values_list('quantity', flat=True)[0]
        )
        return quantity_sum or 0


class PantryProduct(BaseModel):
    product = models.ForeignKey(
        Product,
        verbose_name=_('Product'),
        on_delete=models.CASCADE,
        related_name='pantries_relations',
    )
    pantry = models.ForeignKey(
        Pantry,
        verbose_name=_('Pantry'),
        on_delete=models.CASCADE,
        related_name='products_relations',
    )
    bought_at = models.DateField(_('Bought at'), default=date.today)
    quantity = models.PositiveIntegerField(_('Quantity'))
    expiration_date = models.DateField(_('Expiration date'), null=True)

    objects = PantryProductQuerySet.as_manager()


class PantryProductWithdraw(BaseModel):
    product = models.ForeignKey(
        Product,
        verbose_name=_('Product'),
        on_delete=models.CASCADE,
        related_name='pantries_withdraws',
    )
    pantry = models.ForeignKey(
        Pantry,
        verbose_name=_('Pantry'),
        on_delete=models.CASCADE,
        related_name='products_withdraws',
    )
    quantity = models.IntegerField(_('Quantity'))

    objects = PantryProductQuerySet.as_manager()
