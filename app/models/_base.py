from datetime import datetime

from django.db import models

from core.utils import generate_uuid


class UUIDModel(models.Model):
    id = models.UUIDField(primary_key=True, default=generate_uuid)

    class Meta:
        abstract = True


class BaseModel(UUIDModel, models.Model):
    created_at = models.DateTimeField(default=datetime.now, editable=False)

    class Meta:
        abstract = True
