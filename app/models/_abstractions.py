from copy import deepcopy
from functools import reduce
from typing import List, Iterable

from django.db.models import QuerySet

from app.models._pantry_product import PantryProduct, PantryProductWithdraw


class MergedPantryProductList(list):
    def filter_no_quantity(self):
        return [item for item in self if item.quantity]


class MergedPantryProduct:
    def __init__(self, pantry_product: PantryProduct):
        self.pantry = pantry_product.pantry
        self.product = pantry_product.product
        self.quantity = pantry_product.quantity

    @classmethod
    def merge_by_quantity(
        cls,
        entries: List[PantryProduct] or QuerySet,
        withdraws: List[PantryProductWithdraw] or QuerySet,
    ) -> MergedPantryProductList:
        pantry_product_log = list(entries) + cls._make_quantities_negative(withdraws)
        instances = reduce(cls._merge_reducer, pantry_product_log, {}).values()
        return MergedPantryProductList(instances)

    @staticmethod
    def _merge_reducer(instances_dict, pantry_product: PantryProduct) -> dict:
        product_id = pantry_product.product_id

        if product_id in instances_dict:
            instances_dict[product_id].quantity += pantry_product.quantity
        else:
            instances_dict[product_id] = MergedPantryProduct(pantry_product)

        return instances_dict

    @staticmethod
    def _make_quantities_negative(withdraws: Iterable) -> list:
        _withdraws = []

        for withdraw in withdraws:
            _withdraw = deepcopy(withdraw)
            _withdraw.quantity = -_withdraw.quantity
            _withdraws.append(_withdraw)

        return _withdraws
