from django.db import models
from django.utils.translation import ugettext as _

from app.models._base import BaseModel
from app.models._pantry import Pantry
from app.models._user import User


class Product(BaseModel):
    name = models.CharField(_('Name'), max_length=30)
    bar_code = models.CharField(_('Bar code'), max_length=100)
    image = models.ImageField(_('Image'), upload_to='products', default='products/placeholder.png')
    created_by = models.ForeignKey(
        User,
        verbose_name=_('Created by'),
        on_delete=models.CASCADE,
        related_name='created_products',
    )


class PantryUser(BaseModel):
    pantry = models.ForeignKey(
        Pantry,
        verbose_name=_('Pantry'),
        on_delete=models.CASCADE,
        related_name='users_relations',
    )
    user = models.ForeignKey(User, verbose_name=_('User'), on_delete=models.CASCADE, related_name='pantries_relations')
    is_admin = models.BooleanField()


class Store(BaseModel):
    name = models.CharField(_('Name'), max_length=30, unique=True)
    created_by = models.ForeignKey(
        User,
        verbose_name=_('Created by'),
        on_delete=models.CASCADE,
        related_name='created_stores',
    )


class StoreProduct(BaseModel):
    product = models.ForeignKey(
        Product,
        verbose_name=_('Product'),
        on_delete=models.CASCADE,
        related_name='stores_relations',
    )
    store = models.ForeignKey(
        Store,
        verbose_name=_('Store'),
        on_delete=models.CASCADE,
        related_name='products_relations',
    )
    price = models.FloatField(_('Price'))
