from django.db import models
from django.db.models import QuerySet
from django.utils.translation import ugettext as _

from app.models._base import BaseModel
from app.models._user import User


class PantryQueryset(QuerySet):
    def with_products(self) -> QuerySet:
        return self.filter(products_relations__isnull=False)

    def filter_by_user(self, user: User) -> QuerySet:
        return self.filter(users_relations__user=user)


class Pantry(BaseModel):
    name = models.CharField(_('Name'), max_length=30)

    objects = PantryQueryset.as_manager()
