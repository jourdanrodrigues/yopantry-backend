from django.contrib.auth.models import AbstractUser, UserManager as DjangoUserManager
from django.db import models
from django.db.models import QuerySet
from django.utils.translation import ugettext as _

from app.models._base import UUIDModel


class UserQueryset(QuerySet):
    def with_pantries(self):
        return self.filter(pantries_relations__isnull=False)

    def that_has_pantries_with_products(self):
        return self.filter(pantries_relations__pantry__products_relations__isnull=False)


class UserManager(DjangoUserManager.from_queryset(UserQueryset)):
    pass


class User(UUIDModel, AbstractUser):
    email = models.EmailField(
        _('email address'),
        unique=True,
        error_messages={'unique': _('A user with that email already exists.')},
    )

    objects = UserManager()
