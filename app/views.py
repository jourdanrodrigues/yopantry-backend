from django.http import HttpRequest
from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from app.filters import NameFilter, NestedPantryRouterFilter
from app.models import Product, Store, Pantry, PantryProduct, MergedPantryProduct, PantryProductWithdraw
from app.permissions import NestedPantryOwner
from app.serializers import (
    PantrySerializer,
    ProductSerializer,
    PantryProductSerializer,
    PantryProductWithdrawSerializer,
    StoreSerializer,
    UserSerializer,
)


class AuthViewSet(viewsets.ViewSet, mixins.CreateModelMixin):
    def create(self, request, *args, **kwargs):
        response = obtain_jwt_token(get_django_request(request), *args, **kwargs)

        if response.status_code == status.HTTP_200_OK:
            response.status_code = status.HTTP_201_CREATED  # It's creating the token

        return response

    @action(methods=['POST'], detail=False)
    def refresh(self, request, *args, **kwargs):
        return refresh_jwt_token(get_django_request(request), *args, **kwargs)

    @action(methods=['POST'], detail=False)
    def register(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)


class ProductViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    filter_backends = (NameFilter,)
    permission_classes = (IsAuthenticatedOrReadOnly,)


class PantryViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    queryset = Pantry.objects.all()
    serializer_class = PantrySerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return self.queryset.filter_by_user(self.request.user)


class StoreViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.CreateModelMixin):
    queryset = Store.objects.all()
    serializer_class = StoreSerializer
    filter_backends = (NameFilter,)
    permission_classes = (IsAuthenticatedOrReadOnly,)


class PantryProductViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):
    queryset = PantryProduct.objects.all()
    permission_classes = (IsAuthenticated, NestedPantryOwner)
    serializer_class = PantryProductSerializer
    filter_backends = (NestedPantryRouterFilter,)

    withdraw_queryset = PantryProductWithdraw.objects.all()
    withdraw_serializer_class = PantryProductWithdrawSerializer

    def get_queryset(self):
        return super().get_queryset().select_related('product', 'pantry')

    def get_withdraw_queryset(self):
        return self.withdraw_queryset.select_related('product', 'pantry')

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context['pantry'] = get_object_or_404(Pantry, id=self.kwargs['pantry_pk'])
        return context

    def get_serializer(self, *args, **kwargs):
        if 'data' in kwargs:
            kwargs['many'] = True
        return super().get_serializer(*args, **kwargs)

    def get_withdraw_serializer(self, *args, **kwargs):
        context = self.get_serializer_context()
        context['product'] = get_object_or_404(Product, id=self.kwargs['pk'])
        return self.withdraw_serializer_class(*args, context=context, **kwargs)

    # noinspection PyUnusedLocal
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        withdraw_queryset = self.filter_queryset(self.get_withdraw_queryset())

        merged_pantry_products = MergedPantryProduct.merge_by_quantity(queryset, withdraw_queryset)
        serializer = self.get_serializer(merged_pantry_products, many=True)
        return Response(serializer.data)

    # noinspection PyUnusedLocal
    def destroy(self, request, *args, **kwargs):
        serializer = self.get_withdraw_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=status.HTTP_204_NO_CONTENT)


def get_django_request(request: Request) -> HttpRequest:
    return getattr(request, '_request')
