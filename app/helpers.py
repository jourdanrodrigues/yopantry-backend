from django.contrib.auth import get_user_model
from rest_framework_jwt.settings import api_settings

User = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


def generate_jwt_token(user: User) -> str:
    return jwt_encode_handler(jwt_payload_handler(user))
