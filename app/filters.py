from django.utils.translation import ugettext as _
from rest_framework.exceptions import ValidationError
from rest_framework.filters import BaseFilterBackend


class NameFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        name = request.query_params.get('name', '')

        if len(name) < 3:
            raise ValidationError(_('Filtering by "name" is required with at least 3 characters.'))

        return queryset.filter(name__icontains=name)


class NestedPantryRouterFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        return queryset.filter(pantry_id=view.kwargs['pantry_pk'])
