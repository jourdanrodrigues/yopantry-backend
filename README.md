# YoPantry

This project runs entirely on Docker containers. Make sure to [have it][docker-download] in your environment.

## Style-guides used

- [Python Enhancement Proposal (PEP) 8][pep-8-link] (with exceptions):
  - 120 characters per line
  - required trailing comma
- [Django/REST][django-rest-style-guide]
- [Commit message style guide][git-commit-message-link]

## Setting up development environment

Before doing anything, run the following:

```bash
./scripts/setup_dev_env.sh
```

To make your life slightly easier, the script [`compose.sh`](compose.sh) is there for you to run commands in your
container. It's just a wrapper for `docker-compose`, so you might want to take a look at
[its documentation][docker-compose-docs].

### Running the server

```bash
./compose.sh dev up # Development environment
./compose.sh up # Production environment
```

### Running the tests

Since you'll probably be running this quite often ~~or not~~, the `test` param translates to `python manage.py test`.

```bash
./compose.sh dev run test # Development environment
./compose.sh run test # Production environment
```

[pep-8-link]: https://www.python.org/dev/peps/pep-0008/
[docker-download]: https://www.docker.com/community-edition#/download
[docker-compose-docs]: https://docs.docker.com/compose/reference/
[django-rest-style-guide]: https://github.com/jourdanrodrigues/django_rest_styleguide
[git-commit-message-link]: https://github.com/slashsBin/styleguide-git-commit-message
