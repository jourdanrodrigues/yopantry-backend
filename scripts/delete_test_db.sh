#!/usr/bin/env bash

docker exec -it yopantry_db psql -U postgres -c 'drop database test_yopantry_db'
