from django.conf import settings
from django.contrib import admin
from django.urls import path, re_path, include
from django.views.static import serve
from rest_framework_swagger.views import get_swagger_view

from app.urls import router, pantry_router

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls + pantry_router.urls)),
    path('docs/', get_swagger_view(title='YoPantry API')),
]

if not settings.PRODUCTION:
    urlpatterns += [
        re_path('media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
    ]
