from django.test import SimpleTestCase

from core.utils import generate_uuid


class TestFunction(SimpleTestCase):
    def test_that_it_returns_uuid(self):
        value = generate_uuid()
        self.assertRegex(value, r'[a-z0-9]{8}(-[a-z0-9]{4}){3}-[a-z0-9]{12}')
