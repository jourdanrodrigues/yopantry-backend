import sentry_sdk

from core.settings._environment import SENTRY_DSN, SENTRY_ENV

sentry_sdk.init(dsn=SENTRY_DSN, environment=SENTRY_ENV)
