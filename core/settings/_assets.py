import os

from core.settings._environment import BASE_DIR, PRODUCTION, AWS_STORAGE_BUCKET_NAME

AWS_DEFAULT_ACL = None  # Defined here for silent storage tests

if PRODUCTION:
    S3_USE_SIGV4 = True

    AWS_S3_REGION_NAME = 'sa-east-1'
    AWS_S3_HOST = 's3.amazonaws.com'

    bucket_host = '//{}.{}/{}'.format(AWS_S3_REGION_NAME, AWS_S3_HOST, AWS_STORAGE_BUCKET_NAME)

    MEDIA_URL = bucket_host + 'media/'
    STATIC_URL = bucket_host + 'static/'

    DEFAULT_FILE_STORAGE = 'core.storage_backends.MediaStorage'
    STATICFILES_STORAGE = 'core.storage_backends.StaticStorage'
else:
    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(BASE_DIR, 'assets', 'media')

    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join(BASE_DIR, 'assets', 'static')
