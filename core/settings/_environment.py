import os

from core.utils import DotEnvReader

__all__ = [
    'BASE_DIR',
    'SECRET_KEY',
    'PRODUCTION',
    'DEBUG',
    'SENTRY_DSN',
    'SENTRY_ENV',
    'ALLOWED_CLIENTS',
    'ALLOWED_HOSTS',
    'AWS_ACCESS_KEY_ID',
    'AWS_SECRET_ACCESS_KEY',
    'AWS_STORAGE_BUCKET_NAME',
]

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

DotEnvReader(os.path.join(BASE_DIR, '.env')).read()

SECRET_KEY = os.getenv('SECRET_KEY')

PRODUCTION = bool(int(os.getenv('PRODUCTION', 0)))
DEBUG = bool(int(os.getenv('DEBUG', not PRODUCTION)))

ALLOWED_HOSTS = os.getenv('ALLOWED_HOSTS', '*' if not PRODUCTION else '').split(',')
ALLOWED_CLIENTS = os.getenv('ALLOWED_CLIENTS', '').split(',')

SENTRY_DSN = os.getenv('SENTRY_DSN')
SENTRY_ENV = os.getenv('SENTRY_ENV')

AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY')
AWS_STORAGE_BUCKET_NAME = os.getenv('AWS_STORAGE_BUCKET_NAME')

if PRODUCTION:
    error_message = 'The {} must be set in the environment'
    if not ALLOWED_CLIENTS:
        raise EnvironmentError(error_message.format('variable "ALLOWED_CLIENTS"'))
    if not (AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY and AWS_STORAGE_BUCKET_NAME):
        raise EnvironmentError(error_message.format('AWS variables'))
    if not (SENTRY_DSN and SENTRY_ENV):
        raise EnvironmentError(error_message.format('Sentry variables'))
